﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour
{
    public GameObject Potion;

    private Rigidbody rb;
    // Update is called once per frame
    void Update()
    {
        
    }
    public void addForceObject()
    {
        rb.AddForce(transform.forward * 1);
    }
    IEnumerator RandomTime()
    {
        Instantiate(Potion);
        yield return new WaitForSeconds(2f);
    }
}
