﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    private float timer = 0.0f;

    public static bool startgame;
    // Start is called before the first frame update
    void Start()
    {
        startgame = false;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= 5)
        {
            startgame = true;
            Destroy(this.gameObject);
        }
    }
}
