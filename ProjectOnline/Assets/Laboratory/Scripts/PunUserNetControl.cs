﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
//Lab CAS
using Hashtable = ExitGames.Client.Photon.Hashtable;
/////////
using Photon.Realtime;

[RequireComponent(typeof(PhotonTransformView))]
public class PunUserNetControl : MonoBehaviourPunCallbacks , IPunInstantiateMagicCallback
{
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;

    public SkinnedMeshRenderer _teamRender;
    #region Photon Callback

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        ChangeColorProperties();
        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());
        //PhotonNetwork.LocalPlayer.
        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
        if (photonView.IsMine) {
            LocalPlayerInstance = gameObject;
            info.Sender.TagObject = this.gameObject;
            //Call CustomProperties to other object.
            //ChangeColorProperties();
            GetComponentInChildren<UIPlayerInfoManager>().SetLocalUI();
        }
        else {
            GetComponentInChildren<Camera>().enabled = false;
            GetComponentInChildren<AudioListener>().enabled = false;
            //GetComponent<FirstPersonController>().enabled = false;
            GetComponent<MoveBehaviour>().enabled = false;
            GetComponent<BasicBehaviour>().enabled = false;
            GetComponent<AimBehaviourBasic>().enabled = false;

            //Apply CustomProperties to other object.
            OnPlayerPropertiesUpdate(photonView.Owner, photonView.Owner.CustomProperties);
        }

        //setting mesh team
        if (_teamRender != null)
        {
            SettingPlayerTeam(info.Sender);
        }
        
        GetComponentInChildren<UIPlayerInfoManager>().SetNickName(info.Sender.NickName);
        //Fix setting for Game Network State Management
        // #Critical
        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
        DontDestroyOnLoad(gameObject);
    }

    #endregion

    void Update()
    {
        if (!photonView.IsMine)
            return;

        //Lab CAS
        // if (Input.GetKeyDown(KeyCode.C)) {
        //     ChangeColorProperties();
        // }
        /////////
    }
    //Lab CAS
    private void ChangeColorProperties()
    {
        Hashtable props = new Hashtable
        {
            {PunGameSetting.PLAYER_COLOR, Random.Range(0,7)}
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
    }

    public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(target, changedProps);
        if (changedProps.ContainsKey(PunGameSetting.PLAYER_COLOR) &&
            target.ActorNumber == photonView.ControllerActorNr)
        {
            object colors;
            if (changedProps.TryGetValue(PunGameSetting.PLAYER_COLOR, out colors))
            {
                GetComponentInChildren<SkinnedMeshRenderer>().material.color = PunGameSetting.GetColor((int)colors);
            }

            return;
        }
    }

    private void SettingPlayerTeam(Player Sender)
    {
        PhotonTeam _currenTeam = PhotonTeamExtensions.GetPhotonTeam(Sender);
        if (_currenTeam!=null)
        {
            int colors = (int) _currenTeam.Code;
            _teamRender.material.color = PunGameSetting.GetColor(colors);
        }
    }
    /////////

}
