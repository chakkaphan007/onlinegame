﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jump : MonoBehaviour
{
    private Rigidbody rb;
    public Vector3 impulse = new Vector3(5000.0f, 1000.0f, 0.0f);
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            print("jump");
            other.gameObject.GetComponent<Rigidbody>().AddForce(impulse, ForceMode.Impulse);
        }
    }
}
