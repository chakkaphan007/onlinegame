﻿using System;
using UnityEngine;
using Photon.Pun;

public class PunPlayerController : MonoBehaviourPun
{
    public Rigidbody bulletPrefab;
    public Rigidbody Attackbullet;
    public Camera m_playerCam;
    public static Animator animator;
    public GameObject Spawner;
    private PhotonView PV;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
        Cursor.visible = false;
        if (!PV.IsMine)
        {
            Destroy(GetComponentInChildren<Camera>().gameObject);
        }

        if (PV.IsMine)
        {
            //m_playerCam = this.GetComponentInChildren<Camera>();
            animator = GetComponent<Animator>();
        }
    }

    void Update()  {
        if (!PV.IsMine)
        {
            return;
        }

        Spawner.transform.rotation = m_playerCam.transform.rotation ;
        if (Input.GetMouseButtonDown(0))
        {
            animator.SetTrigger("Attack");
        }
        // if (Input.GetKeyDown(KeyCode.Mouse1))
        // {
        //     //CmdFire(m_playerCam.transform.rotation);
        // }
    }

    // void CmdFire(Quaternion rotation)  {
    //     //Keep Data when Instantiate.
    //     object[] data = { photonView.ViewID };
    //
    //     // Spawn the bullet on the Clients and Create the Bullet from the Bullet Prefab
    //      PhotonNetwork.Instantiate(this.bulletPrefab.name
    //                                             , this.transform.position + (this.transform.forward * 0.5f)
    //                                             , rotation
    //                                             , 0
    //                                             , data);
    // }
    public void UseSkill()
    {
        if (!PV.IsMine)
        {
            return;
        }
        object[] data = { photonView.ViewID };
        PhotonNetwork.Instantiate(this.bulletPrefab.name, 
            Spawner.transform.position + (Spawner.transform.forward * 0.2f),
            Spawner.transform.rotation
            , 0
            , data);
    }

    public void normalAttack()
    {
        if (!PV.IsMine)
        {
            return;
        }
        object[] data = { photonView.ViewID };
        PhotonNetwork.Instantiate(this.Attackbullet.name,
            Spawner.transform.position + (Spawner.transform.forward * 0.2f),
            Spawner.transform.rotation
            , 0
            , data);
    }
}
